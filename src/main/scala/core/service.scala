package core

import akka.actor.Actor
import shapeless._
import Tuples._
import scala.reflect._
import spray.json._
import DefaultJsonProtocol._
import scalax.io.JavaConverters._
import scalax.file.Path
import scala.io.Source

/**
 * We use the companion object to hold all the messages that the ``RegistrationActor``
 * receives.
 */
object ServiceActor {
  object nodePoly extends Poly1 {
   implicit def caseXmlNode = at[scala.xml.NodeSeq](_.text)
  }
  
  case class Entry(
    id: String,
    published: String,
    updated: String,
    title: String,
    summary: String,
    image: String,
    album: String,
    author: String,
    thumbnail: String,
    url: String)

  case class EntriesResponse(
    entries: List[Entry],
    total: Long,
    current: Long,
    real: Long)

  class Service(index: Int = 1, query: String = "paisagens") {
    val url = s"http://picasaweb.google.com/data/feed/base/all?max-results=50&start-index=$index&thumbsize=150c&orderby=date&alt=atom&q=" + java.net.URLEncoder.encode(query)
    println(url)
    var xml: scala.xml.Elem = null
    println("requesting")
    val rawXml = Source.fromURL(url, "UTF-8").mkString
    println("requested")
    println("parsing")
    xml = scala.xml.XML.loadString(rawXml)
    println("parsed")
    println("processing")

    implicit val entryFormat = jsonFormat10(Entry)

    val entries = ((xml \ "entry").map { e =>
      Entry.tupled((e \ "id",
        e \ "published",
        e \ "updated",
        e \ "title",
        e \ "summary",
        e \ "content" \ "@src",
        (e \ "link").filter(x => (x \ "@rel").text == "alternate") \ "@href",
        e \ "author" \ "name",
        (e \ "group" \ "thumbnail" \ "@url"),
        (e \ "link").filter(x => (x \ "@rel").text == "http://schemas.google.com/photos/2007#canonical") \ "@href")
      .hlisted.map(nodePoly).tupled)
    }).toList

    println("processed")

    lazy val json = {
      implicit val entryFormat = jsonFormat10(Entry)
      implicit val entriesResponseFormat = jsonFormat4(EntriesResponse)
      val response = new EntriesResponse(entries, 
        ((xml \ "totalResults").text match {
          case "" => "0"
          case x: String => x
          }).toLong, 
        ((xml \ "itemsPerPage").text match {
          case "" => "0"
          case x: String => x
        }).toLong,
        (xml \ "entry").length)
      response.toJson.toString()
    }

  }

}

case class SearchService(index: Int = 1, query: String = "paisagens")

/**
 * Registers the users. Replies with
 */
class ServiceActor extends Actor {
  import ServiceActor._

  def receive: Receive = {
    case e: SearchService => sender ! new Service(e.index, e.query).json
  }

}
