package api

import spray.routing.Directives
import scala.concurrent.ExecutionContext
import akka.actor.ActorRef
import core.{ ServiceActor, SearchService }
import akka.util.Timeout
import spray.http._
import scala.Some
import spray.http.MediaTypes._
import akka.actor.ActorRefFactory
import spray.httpx.marshalling.Marshaller
import spray.routing.HttpService

class TheService(service: ActorRef)(implicit executionContext: ExecutionContext)
  extends Directives with DefaultJsonFormats {

  import akka.pattern.ask
  import scala.concurrent.duration._
  implicit val timeout = Timeout(10.seconds)

  val route = {
    get {
      path("data") {
        parameters('index.as[Int] ? 1, 'query.as[String] ? "paisagens").as(SearchService) { ss =>
          respondWithMediaType(`application/json`) {
            handleWith { s: String =>
              (service ? ss).mapTo[String]
            }
          }
        }
      }
    }
  }

}